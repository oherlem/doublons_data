from utils import utils

json_path = 'json_data/impacters_edpr_nord_v0-1.json'
out_path = "out/edpr_nord_random_pairs"

utils.wrapper_data_frame_random_pairs(json_path, out_path, window=10, num_pairs=200)