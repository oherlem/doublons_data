import pandas as pd
import itertools as it
import json
from fuzzywuzzy import fuzz
import random
# from fuzzywuzzy import process

def wrapper_dataframe_candidate(file_path: str, path_out: str, window: int, thresh_r: float, thresh_pr: float):
    """
    Wrapper function to make and write a dataframe of potential duplicate
    candidates.
    :param file_path: json file name
    :param file_out: file path to output files
    :return: a dataframe
    """

    file_out = path_out + ('_%s_%s' % (thresh_pr, thresh_r))
    file_out_csv = file_out + ".csv"
    file_out_jsonl = file_out + ".jsonl"

    data_json = load_article_data(file_path)[:1500]
    list_cand = make_list_cand(data_json, window, thresh_r, thresh_pr)
    unique_sets = make_unique_sets(list_cand)

    df_cand = make_candidates_dataframe(list_cand, unique_sets, data_json)

    # write csv

    df_cand.to_csv(file_out_csv)

    # write jsonl for prodigy

    data_json_out = df_cand[['id_combi']]

    data_json_out['text'] = '###### TEXT A #######\n\n' + \
                        df_cand['full_text_x'] + \
                        '\n\n###### TEXT B #######\n\n' + \
                        df_cand['full_text_y']

    data_json_out.to_json(file_out_jsonl, orient="records", lines=True)

def wrapper_data_frame_random_pairs(file_path: str, path_out: str, window: int, num_pairs: int = 80):

    file_out_csv = path_out + ".csv"
    file_out_jsonl = path_out + ".jsonl"

    data_json = load_article_data(file_path)[:1500]
    list_pairs = make_list_subset_random_pairs(data_json, window, num_pairs)

    df_pairs = make_random_pairs_dataframe(list_pairs, data_json)

    # write csv

    df_pairs.to_csv(file_out_csv)

    # write jsonl for prodigy

    data_json_out = df_pairs[['id_combi']]

    data_json_out['text'] = '###### TEXT A #######\n\n' + \
                        df_pairs['full_text_x'] + \
                        '\n\n###### TEXT B #######\n\n' + \
                        df_pairs['full_text_y']

    data_json_out.to_json(file_out_jsonl, orient="records", lines=True)

def load_article_data(file_path : str) -> pd.DataFrame:
    """
    JSON loader function
    :param file_path: path to json file
    :return: a dataframe of press articles with their id, title, and text
    """

    with open(file_path) as file:
        data = json.load(file)

    df_articles = pd.DataFrame(data['documents'])[['id', 'full_text', 'title', 'date']]
    df_articles = df_articles[df_articles.full_text.notnull()]
    df_articles['date'] = pd.to_datetime(df_articles['date'], format='%Y-%m-%d')
    df_articles = df_articles.sort_values('date').reset_index()

    return df_articles

def make_list_cand(data: pd.DataFrame, window: int, thresh_r: float, thresh_pr: float) -> list:
    """
    This function examines every 2-article combination, determines whether
    they could be potential duplicates, and appends and the duplicate candidates
    to a list.
    The criteria to select potential duplicates is based on the Levenhstein distance
    between each of the paragraphs of the two articles.
    :param data: a dataframe of press articles
    :param thresh_r : threshold of the string matching ratio
    :param thresh_pr : threshold the string partial matching ratio
    :return: a list of couples of potential duplicates
    """
    out_list = []

    time_window_list = make_time_restricted_list(data, window)

    for id_combi in time_window_list:
        l_str_1 = data[data['id'] == id_combi[0]]['full_text'].to_string().split(sep="\n\n")
        l_1 = [x for x in l_str_1 if par_condition(x)]

        l_str_2 = data[data['id'] == id_combi[1]]['full_text'].to_string().split(sep="\n\n")
        l_2 = [x for x in l_str_2 if par_condition(x)]

        for i in list(it.product(*[l_1, l_2])):
            if fuzz.partial_ratio(i[0], i[1]) > thresh_pr:
                out_list.append(id_combi)
                break
            if fuzz.ratio(i[0], i[1]) > thresh_r:
                out_list.append(id_combi)
                break

    return out_list

def make_list_subset_random_pairs(data: pd.DataFrame, window: int, num_pairs: int) -> list:

    time_window_list = make_time_restricted_list(data, window)

    out_list = random.sample(time_window_list, k=num_pairs)

    return out_list





def make_time_restricted_list(data: pd.DataFrame, window: int) -> list:
    """
    This function selects only combinations of articles that are published `window` days apart.
    :param data:
    :param window:
    :return:
    """

    out_list = []

    for i in data['id'].index:
        for j in range(i + 1, len(data['id'])):
            date_1 = data.loc[i]['date']
            date_2 = data.loc[j]['date']
            if abs((date_1 - date_2).days) < window:
                combi = (data.loc[i]['id'], data.loc[j]['id'])
                out_list.append(combi)
            else:
                break

    return out_list


def par_condition(par: str, min_len: int = 20) -> list:
    """
    This function returns a condition to select paragraphs from an article
    :param par: a string
    :param min_len: an integer, the minimum length of a character string to be considered
    :return: a boolean
    """

    cond_1 = len(par) > min_len
    cond_2 = '@' not in par

    out_cond = cond_1 and cond_2
    return out_cond

def make_unique_sets(list_cand: list) -> list:
    """
    This function examines the list of potential duplicates, and groups them together
    by transitivity. I.e. if (A, B) and (A,C) are potential duplicates, then A,B, and C
    are potential triplets. Then they will belong to the same group of potential duplicates
    that contains the set {A, B, C}.
    :param list_cand: initial list of potential duplicates
    :return: a list of mutually exclusive sets where potential duplicates are grouped together
    """
    doublon = {}

    for couple in list_cand:
        try:
            doublon[couple[0]].add(couple[1])
        except KeyError:
            doublon[couple[0]] = set([couple[0]])
            doublon[couple[0]].add(couple[1])

    for key in doublon:
        for i in doublon[key]:
            for key_2 in doublon:
                if bool(doublon[key].intersection(doublon[key_2])):
                    doublon[key] = doublon[key_2] = doublon[key] | doublon[key_2]

    unique_sets = [list(i) for i in set([frozenset(doublon[key]) for key in doublon])]

    return unique_sets

def make_candidates_dataframe(list_cand: list, unique_sets: list, data_json: pd.DataFrame) -> pd.DataFrame:
    """
    This function makes a dataframe of potential duplicates that are grouped together.
    :param list_cand: a list of couple of articles that are potential candidates.
    :param unique_sets: a list of mutually exclusive sets that groups the duplicate candidates.
    :param data_json: a dataframe with id, title, and full text of the press articles
    :return: a data frame where the titles and texts of potential candidates are on the same row.
    """
    groups = [ind for i in list_cand for ind, j in enumerate(unique_sets) if i[0] in j]

    data_cand = {'id_combi': list_cand,
                 'id_1': [i[0] for i in list_cand],
                 'id_2': [i[1] for i in list_cand],
                 'group_cand': groups}

    df_cand_init = pd.DataFrame(data_cand)

    df_cand_merg_1 = df_cand_init.merge(data_json[['title', 'full_text', 'id']],
                                        left_on='id_1', right_on='id',
                                        how='left')

    df_cand_merg_2 = df_cand_merg_1.merge(data_json[['title', 'full_text', 'id']],
                                          left_on='id_2', right_on='id',
                                          how='left')

    df_cand = df_cand_merg_2[['group_cand', 'id_combi', 'title_x',
                              'title_y', 'full_text_x', 'full_text_y']]

    return df_cand


def make_random_pairs_dataframe(list_art: list, data_json: pd.DataFrame) -> pd.DataFrame:
    """
    This function makes a dataframe of random pairs of articles.
    :param list_art: a list of couple of articles that are potential candidates.
    :param data_json: a dataframe with id, title, and full text of the press articles
    :return: a data frame where the titles and texts of potential candidates are on the same row.
    """
    data_cand = {'id_combi': list_art,
                 'id_1': [i[0] for i in list_art],
                 'id_2': [i[1] for i in list_art]}

    df_cand_init = pd.DataFrame(data_cand)

    df_cand_merg_1 = df_cand_init.merge(data_json[['title', 'full_text', 'id']],
                                        left_on='id_1', right_on='id',
                                        how='left')

    df_cand_merg_2 = df_cand_merg_1.merge(data_json[['title', 'full_text', 'id']],
                                          left_on='id_2', right_on='id',
                                          how='left')

    df_cand = df_cand_merg_2[['id_combi', 'title_x',
                              'title_y', 'full_text_x', 'full_text_y']]

    return df_cand
